//
//  SecondViewController.swift
//  TaskPattern
//
//  Created by Sierra 4 on 14/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
    }
}
