//
//  commonFunctions.swift
//  TaskPattern
//
//  Created by Sierra 4 on 14/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

extension ViewController{
    func errorMessageForLAErrorCode( _ errorCode:Int ) -> String{
        var message = nilStrings.nilString.rawValue
        switch errorCode {
        case LAError.Code.appCancel.rawValue:
            message = touchIdMessages.authCancelApp.rawValue
        case LAError.Code.authenticationFailed.rawValue:
            message = touchIdMessages.credentials.rawValue
        case LAError.Code.invalidContext.rawValue:
            message = touchIdMessages.invalidContext.rawValue
        case LAError.Code.passcodeNotSet.rawValue:
            message = touchIdMessages.passcode.rawValue
        case LAError.Code.systemCancel.rawValue:
            message = touchIdMessages.authCancelSyst.rawValue
        case LAError.Code.touchIDLockout.rawValue:
            message = touchIdMessages.fail.rawValue
        case LAError.Code.touchIDNotAvailable.rawValue:
            message = touchIdMessages.touchId.rawValue
        case LAError.Code.userCancel.rawValue:
            message = touchIdMessages.userCancel.rawValue
            
        case LAError.Code.userFallback.rawValue:
            message = touchIdMessages.fallBack.rawValue
            
        default:
            message = touchIdMessages.LAerror.rawValue
        }
        return message
    }
}
