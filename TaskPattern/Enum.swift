//
//  Enum.swift
//  TaskPattern
//
//  Created by Sierra 4 on 14/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
enum PasswordLock: Int {
    case Normal
    case Correct
    case Wrong
}
enum strings : String{
    case colon = ":"
}
enum identifiers : String{
    case storyId = "StoryBoardId"
}
enum patternUnlocking : String{
    case pattern = "[0][1][3][4][5][7][8]"
    case gotPassword = "Got Password: "
}
enum dotimages : String{
    case normalDot = "Circled Dot_50"
    case heighlightDot = "DOT Filled_50"
}
enum alertMessages : String {
    case ok = "Ok"
    case error = "Error"
}
enum nilStrings : String{
    case nilString = ""
}
enum touchIdMessages : String{
    case welcome = "Welcome Here"
    case allowed = "Only awesome people are allowed"
    case sensor = "This device does not have a TouchID sensor."
    case authCancelApp = "Authentication was cancelled by application"
    case credentials = "The user failed to provide valid credentials"
    case invalidContext = "The context is invalid"
    case passcode = "Passcode is not set on the device"
    case authCancelSyst = "Authentication was cancelled by the system"
    case fail = "Too many failed attempts."
    case touchId = "TouchID is not available on the device"
    case userCancel = "The user did cancel"
    case fallBack = "The user chose to use the fallback"
    case LAerror = "Did not find error code on LAError object"

}
