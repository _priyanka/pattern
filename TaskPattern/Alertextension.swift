//
//  Alertextension.swift
//  TaskPattern
//
//  Created by Sierra 4 on 14/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
func alertBox( _ title:String, message:String ) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: alertMessages.ok.rawValue, style: .default, handler: nil)
    alert.addAction(okAction)
    DispatchQueue.main.async { () -> Void in
        self.present(alert,animated: true, completion: nil)
    }
}
}
