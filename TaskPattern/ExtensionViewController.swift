//
//  ExtensionViewController.swift
//  TaskPattern
//
//  Created by Sierra 4 on 14/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit
import HUIPatternLockView_Swift
import LocalAuthentication

// MARK: - Custom LockView with images
extension ViewController {
    func load(){
        lockViewImagesConfigure()
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        timeLabel.text = String(hour) + strings.colon.rawValue + String(minutes)
        let context = LAContext()
        var error:NSError?
        guard context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            showAlertViewIfNoBiometricSensorHasBeenDetected()
            return
        }
        context.evaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: touchIdMessages.allowed.rawValue,
            reply: { [unowned self] (success, error) -> Void in
                if( success ) {
                    self.alertBox(alertMessages.error.rawValue, message: touchIdMessages.welcome.rawValue)
                }else {
                    if let error = error {
                        let message = self.errorMessageForLAErrorCode(error._code)
                        self.showAlertViewAfterEvaluatingPolicyWithMessage(message)
                    }
                }
        })
    }
    
    // MARK: - Custom LockView Drawing
    private func lockViewColor(_ state: PasswordLock, highlightedColorUsed: Bool) -> UIColor {
        switch state {
        case PasswordLock.Correct:
            return UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 1.0)
        case PasswordLock.Wrong:
            return UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
        default:
            if highlightedColorUsed {
                return HUIPatternLockView.lineColorDefault
            }
            else {
                return UIColor.black
            }
        }
    }

    internal func lockViewImagesConfigure() {
        let lineColorDefault = HUIPatternLockView.lineColorDefault
        let lineColorcorrect = UIColor.green
        let lineColorWrong = UIColor.red
        let imageNormal = UIImage(named: dotimages.normalDot.rawValue)
        let imageHeighlighted = UIImage(named: dotimages.heighlightDot.rawValue)
        let correctImage = imageHeighlighted?.imageTint(tintColor: lineColorcorrect)
        let wrongImage = imageHeighlighted?.imageTint(tintColor: lineColorWrong)
        lockView.didDrawPatternPassword = { (lockView, count, password) -> Void in
            guard count > 0 else {
                return
            }
            let unlockPassword = patternUnlocking.pattern.rawValue
            if password == unlockPassword {
                lockView.lineColor = lineColorcorrect
                lockView.normalDotImage = correctImage
                lockView.highlightedDotImage = correctImage
                let objHomeVC = self.storyboard?.instantiateViewController(withIdentifier: identifiers.storyId.rawValue)
                self.present(objHomeVC!, animated: true, completion: nil)
            }
            else {
                lockView.lineColor = lineColorWrong
                lockView.normalDotImage = wrongImage
                lockView.highlightedDotImage = wrongImage
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                lockView.resetDotsState()
                lockView.lineColor = lineColorDefault
                lockView.normalDotImage = imageNormal
                lockView.highlightedDotImage = imageHeighlighted
            }
        }
    }
    
    
    // MARK: - Show Alert Sensor
    func showAlertViewIfNoBiometricSensorHasBeenDetected(){
        alertBox(alertMessages.error.rawValue, message: touchIdMessages.sensor.rawValue)
    }
    
    func showAlertViewAfterEvaluatingPolicyWithMessage( _ message:String ){
        alertBox(alertMessages.error.rawValue, message: message)
    }
}

// MARK: - UIImage Tint Color
extension UIImage {
    public func imageTint(tintColor: UIColor) -> UIImage? {
        return imageTint(tintColor, blendMode: .destinationIn)
    }
    
    public func gradientTintImage(tintColor: UIColor) -> UIImage? {
        return imageTint(tintColor, blendMode: .overlay)
    }
    
    public func imageTint(_ tintColor: UIColor, blendMode: CGBlendMode) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        let bounds = CGRect(origin: CGPoint.zero, size: size)
        tintColor.setFill()
        UIRectFill(bounds)
        
        draw(in: bounds, blendMode: blendMode, alpha: 1.0)
        
        //draw again to save alpha channel
        if blendMode != .destinationIn {
            draw(in: bounds, blendMode: .destinationIn, alpha: 1.0)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
